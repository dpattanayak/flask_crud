
from flask import Flask, json, jsonify, request 
from pymongo import MongoClient
from bson import ObjectId


app = Flask(__name__)

client = MongoClient("localhost", 27017)
db = client.flask_crud
users = db.users

@app.route('/adduser', methods=['POST'])
def add_user():

	userdata = json.loads(request.data)	
	print("data form post ==>",userdata)

	for n in range(len(userdata)):
		response = users.insert({"name":userdata[n]['name'],"email":userdata[n]['email']})

	if response:
		output = {'Message':'Inserted Succesfully!'}
	else:
		output = {'Message':'Error Occured While Insert Users'}


	return output

@app.route('/getusers', methods=["GET"])
def get_allusers():

	name = request.args.get('name')
	print("get_user_with_name",name)

	if name:
		query = {"name":name}
	else:
		query = {}

	output = []
	
	for user in users.find(query):
		if user:
			print('User ID ==>',user.get('_id'))
			output.append({	
				'User ID': str(user.get('_id')),			
				'Name' : user['name'],
				'Email ID' : user['email']
			})
		else:
			output.append({'Message':'Error Occured While Insert User'})

	return jsonify({'result' : output})

@app.route('/deleteuser/<string:_id>', methods=['DELETE'])
def delete_user(_id):

	if _id:
		users.delete_one({"_id":ObjectId(_id)})
		output = {'Message':'One user deleted succesfully!'}
	else:
		output = {'Message':'Sorry Cant get the ID of user'}
	
	return output

@app.route('/updateuser/<string:_id>', methods=['PUT'])
def update_user(_id):

	name = request.json['name']
	email = request.json['email']

	print("update_user",name,email)

	if _id:
		users.update({
			"_id":ObjectId(_id)
		},{
			'$set': {
				"name": name,
				"email": email
			}
		})
			
		output = {'Message':'One User Updated Succesfuly!'}

	else:
		
		output = {'Message':'Sorry Cant get the ID of user'}

	return output

if __name__ == "__main__":
    app.run(debug = True)